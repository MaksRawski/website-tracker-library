from selenium.webdriver.remote.webdriver import WebDriver
from selenium.webdriver.remote.webelement import WebElement
from selenium.common.exceptions import NoSuchElementException

from typing import Any, Callable, Union

elementWrapper = Callable[[WebDriver], WebElement]
selectorFunction = Callable[..., elementWrapper]

elementSelector = Union[selectorFunction, str]

'''
in plain english:

elementWrapper -> function which upon executing on WebDriver returns desired WebElement

selectorFunction -> function which takes any argument it might want and returns elementWrapper

elementSelector -> either selectorFunction or simple string
'''
