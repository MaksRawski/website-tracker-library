from selenium.webdriver.remote.webdriver import WebDriver
from selenium.webdriver.remote.webelement import WebElement
from selenium.common.exceptions import NoSuchElementException
from typing import Union

from .types import elementSelector, elementWrapper

# this function is an example of a selectorFunction
def findByText(s: str) -> elementWrapper:
    return lambda driver: __findByText(driver, s)

def __findByText(driver: WebDriver, text: str) -> WebElement:
    return driver.find_element_by_xpath(f"//*[text()='{text}']")


class ElementNotFound(Exception):
    def __init__(self, element=None, message: str=None):
        if message is not None:
            super().__init__(message)
        else:
            super().__init__(f"Element {element} not found")

# ideally this would be a method on top of every elementSelector
# however we can't do that since it requires driver
def getElement(driver: WebDriver, selector: elementSelector) -> Union[WebElement, None]:

    '''
    Gets an element given a selector from a driver.
    If the element isn't found, None is returned.
    '''

    try:
        if isinstance(selector, str):
            return driver.find_element_by_tag_name(selector)
        else:
            return selector(driver)

    except NoSuchElementException as e:
        return None

def getElements(driver: WebDriver, selector: elementSelector) -> Union[list[WebElement], None]:

    '''
    Gets a list of elements given a selector from a driver.
    if no elements are found None is returned.
    '''

    try:
        if isinstance(selector, str):
            elements = driver.find_elements_by_tag_name(selector)
            if len(elements) == 0:
                return None
            else:
                return elements

        else:
            return selector(driver)

    except NoSuchElementException as e:
        return None
