import time
import random
import datetime
import asyncio
import pickle
import yaml
from typing import TypedDict, Optional, Any, Callable, Tuple

from selenium import webdriver
from selenium.webdriver.remote.webelement import WebElement
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

from .message_service import MessageService
from .exceptions import ConditionNotMet


class Credentials(TypedDict):
    username: str
    password: str
    telegramToken: str
    cid: int


def credsFromYaml(file: str) -> Credentials:
    '''
    Loads credentials from a given yaml file.
    Will raise KeyError if it doesn't
    contain any of the following fields:
    - username
    - password
    - telegramToken
    - cid

    Types will be automatically converted to
    what they're supposed to be.
    '''
    with open(file, 'r') as f:
        d = yaml.safe_load(f)
        creds: Credentials = ({
            'username': str(d['username']),
            'password':  str(d['password']),
            'telegramToken': str(d['telegramToken']),
            'cid': int(d['cid'])
        })
        return creds


# TODO: might need to replace all time.sleep with WebDriverWait
class BaseTracker:
    trackerName: str
    loginURL: str
    refreshDelayRange: Tuple[int, int]
    reloginEvery: int
    credentials: Credentials

    headless = True
    logging = True

    _data: list[str] = []

    def __init__(self):
        ops = webdriver.FirefoxOptions()
        ops.headless = self.headless
        self.driver = webdriver.Firefox(options=ops)

        self.messageService = MessageService(self.credentials['telegramToken'], self.credentials['cid'])
        self.loadData()


    # TODO: break this function into smaller chunks
    def _start(self):
        '''
        Starts the pipeline.
        '''
        self._relogin()

        # TODO: relogin every couple hours or detect when on login page
        # TOTHINKABOUT: threading event might be used here instead
        self.running = True
        while self.running:
            self.onRefresh()
            newData = self.getData()

            # if we are running this for the first time
            # then we just save the new data and don't notify about it
            if self._data == []:
                pass

            # TODO: put this check into function so that it can be overidden
            # checks whether newData has elements which self._data doesn't
            elif set(newData) - set(self._data) != set():
                msg = self.generateMessage(newData)
                asyncio.run( self.messageService.send(msg) )

            self._data = newData

            # TODO: this looks horrible, do something about it

            # wait random delay from self.refreshDelayRange seconds
            time.sleep( random.randint(*self.refreshDelayRange) )

            currentTime = time.time()

            if currentTime - self.lastRelogin > self.reloginEvery or self.driver.current_url == self.loginURL:
                self._relogin()
                continue
            self.driver.refresh()


    def _relogin(self):
        self.driver.get(self.loginURL)
        self.login()
        self.log(f"logged in")
        self.lastRelogin = time.time()


    def start(self):
        '''
        This can be modified to change the behavior
        of how the code panics.
        '''
        # TODO: if the code throws the same exception eg. 3 times in a row
        # consider sending some sort of notification
        try:
            self._start()
        except Exception as e:
            print(e)
            self.driver.save_screenshot("exception.png")
            self.quit()


    def login(self):
        '''
        Implementation of this method should assume
        that it's already on the loginURL so it doesn't
        need to manually get it.
        '''


    def loginFromFields(self, usernameSelector: str, passwordSelector: str):
        usernameField = self.driver.find_element_by_css_selector(usernameSelector)
        passwordField = self.driver.find_element_by_css_selector(passwordSelector)

        usernameField.send_keys(self.credentials['username'])
        passwordField.send_keys(self.credentials['password'])

        passwordField.submit()


    def waitForElement(self, selector: str, timeout: int = 30) -> WebElement:
        '''
        Waits timeout seconds for an element given as selector
        to be found after which it will be returned.
        If the element isn't found TimeoutException will be raised.
        First match will be chosen if there are multiple elements available.
        If that's a possible case consider using waitForElements.
        '''
        WebDriverWait(self.driver, timeout).until(
            EC.presence_of_element_located((By.CSS_SELECTOR, selector))
        )
        return self.driver.find_element_by_css_selector(selector)


    def waitForElements(self, selector: str, timeout: int = 30) -> list[WebElement]:
        '''
        Waits timeout seconds for elements given as selector
        to be found after which they will be returned in a list.
        If no elements are found TimeoutException will be raised.
        '''
        WebDriverWait(self.driver, timeout).until(
            EC.presence_of_all_elements_located((By.CSS_SELECTOR, selector))
        )
        return self.driver.find_elements_by_css_selector(selector)


    def getData(self) -> list[str]:
        '''
        Gets data from refreshURL.
        It always has to be implemented manually.
        '''


    def onRefresh(self):
        '''
        (optional) Executes actions which need to be done every time
        the website is refreshed to make the desired elements visible/scrapable.
        waitForElement is what you may want to use here.
        '''


    def generateMessage(self, data: list[str]) -> str:
        '''
        Converts data into a string which can be sent.
        '''
        return f'{self.trackerName}:\n' + '\n\n'.join(data)


    def log(self, msg: str):
        if self.logging is False:
            return

        logMessage = f"[{datetime.datetime.now()}] {self.trackerName} - {msg}"

        print(logMessage)
        with open("logs.txt", "a") as f:
            f.write(logMessage + '\n')


    def saveData(self):
        try:
            with open(f"{self.trackerName}.pkl", "wb") as f:
                pickle.dump(self._data, f)
        except:
            self.log("failed to save data")
        else:
            self.log("succesfully saved data")


    def loadData(self):
        try:
            with open(f"{self.trackerName}.pkl", "rb") as f:
                self._data = pickle.load(f)

        except FileNotFoundError:
            self.log(f"{self.trackerName}.pkl not found")

        except:
            self.log("failed to load data")
            self.running = False
            self.driver.quit()

        else:
            self.log(f"succesfully loaded data")


    def quit(self):
        '''
        Has to be manually called when the object is no longer needed.
        '''
        self.log("Exiting")
        self.running = False
        # TODO: ideally we would like to wait for the current cycle to end
        # and only then move on to saving the data threading.Event might be useful here

        self.saveData()
        # self.driver.close()
        self.driver.quit()


# TODO: change name, move to different file
class TrackersGenerator:
    trackers: list[BaseTracker]

    def __init__(self, trackerDefinition: BaseTracker, file: str):
        with open(file, 'r') as f:
            credentials = [ l.split(":") for l in f.readlines() ]

        for [username, password] in credentials:
            tracker = trackerDefinition()
            tracker.credentials['username'] = username
            tracker.credentials['password'] = password
            self.trackers.append(tracker)

    def start(self):
        for tracker in self.trackers:
            tracker.start()

    def quit(self):
        for tracker in self.trackers:
            tracker.quit()
