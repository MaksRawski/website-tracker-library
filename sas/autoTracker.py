from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from selenium.webdriver.remote.webelement import WebElement

from typing import TypedDict, Optional, Any

from .types import elementSelector
from .funcs import getElement, getElements, ElementNotFound
from .lib import BaseTracker, Credentials

class WebsiteDefinition(TypedDict):
    name: str
    loginUrl: str
    loginPrompt: Optional[elementSelector]
    loginSelector: elementSelector
    dataSelector: elementSelector

class AutoTracker(BaseTracker):
    '''
    implementation of BaseTracker which tries
    to perform all the actions given a simple WebsiteDefinition
    '''
    def __init__(self, website: WebsiteDefinition, creds: Credentials, logging: bool):
        self.websiteDefinition = website
        self.logging = logging

        super().__init__(creds, logging)

    # TODO: remove this
    # left only for debugging purposes!
    def __highlightElement(self, element: WebElement):
        self.driver.execute_script("arguments[0].setAttribute('style', 'border: 2px solid red')", element)


    # TODO: give reason for failure in future
    def verifyData(self) -> bool:
        try:
            self.getLoginForm()
        except:
            return False
        return True


    def getLoginForm(self) -> WebElement:
        self.driver.get(self.websiteDefinition['loginUrl'])

        if self.websiteDefinition['loginPrompt'] is not None:
            prompt = getElement(self.driver, self.websiteDefinition['loginPrompt'])

            if prompt is None:
                raise ElementNotFound(self.websiteDefinition['loginPrompt'])
            else:
                prompt.click()

        selector = getElement(self.driver, self.websiteDefinition['loginSelector'])

        if selector is None:
            iframes = getElements(self.driver, 'iframe')

            if iframes is None:
                raise ElementNotFound(message="loginSelector wasn't found and there are no iframes in which it could be")

            for frame in iframes:
                self.driver.switch_to.frame(frame)
                selector = getElement(self.driver, self.websiteDefinition['loginSelector'])

                if selector is None:
                    self.driver.switch_to.default_content()
                else:
                    return selector
            raise ElementNotFound('loginSelector')

        return selector

    # TODO: allow this to be overwritten
    def login(self):
        selector = self.getLoginForm()

        selector.send_keys(self.creds['username'], Keys.TAB)
        selector = self.driver.switch_to.active_element
        selector.send_keys(self.creds['password'])

        selector.send_keys(Keys.ENTER)

        # TODO: if the url didn't change it means the credentials are wrong
        if self.logging:
            self.log(f"{self.creds['username']} logged in.")

    # TODO: allow this to be overwritten
    def findData(self) -> WebElement:
        dataSelector = getElement(self.driver, self.websiteDefinition['dataSelector'])
        if dataSelector is None:
            raise ElementNotFound(element="dataSelector")
        return dataSelector
