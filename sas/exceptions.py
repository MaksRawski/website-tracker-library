from typing import Callable, Optional
from selenium import webdriver

class ConditionNotMet(Exception):
      def __init__(self, condition: Callable[ [webdriver.Firefox], bool ], exception: Optional[Exception]):
            if exception:
                  super().__init__(f"Condition {condition} raised {exception}")
                  return
            super().__init__(f"Condition {condition} couldn't be fulfilled in given time")
