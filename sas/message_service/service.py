from telegram import Bot
from telegram.error import BadRequest
# from telegram.ext import utils.promise.Promise as Promise

class MessageService():
    def __init__(self, token: str, cid: int):
        self.cid = cid
        self.bot = Bot(token=token)

    async def send(self, msg: str):
        '''
        keep on trying to send until there are no exceptions
        will raise an exception if the request itself is invalid
        '''
        while True:
            try:
                self.bot.send_message(chat_id=self.cid, text=msg)
            except BadRequest as e:
                raise(e)
            except Exception as e:
                pass
            else:
                break
