#!/usr/bin/env python
from distutils.core import setup

setup(name="SAS",
      version="0.1",
      author="Maks Rawski",
      packages=['sas', 'sas.message_service'],
)
