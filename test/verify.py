import asyncio
import threading
from telegram import Update
from telegram.ext import Handler, Updater, MessageHandler

from typing import Union

# TODO: add error handlers
class messageHandler(Handler):
    def __init__(self, cidToWatch, callback):
        '''
        callback function should take a single argument
        which will be the message text
        '''
        self.run_async = True
        self.callback = callback
        self.cid = cidToWatch

    def check_update(self, update) -> bool:
        # handle_update will only be called if the cid of the message is
        # the same as the one used by message_service
        try:
            return update['channel_post']['sender_chat']['id'] == self.cid
        except:
            return False

    def handle_update(self, update, dispatcher, check_result, context=None):
        self.callback( update['channel_post']['text'] )

class VerifyingBot:
    def __init__(self, token: str, cid: int):
        self.updater = Updater(token)
        self.cid = cid
        self.dispatcher = self.updater.dispatcher
        self.handler = messageHandler(self.cid, self.__calback)
        self.callbackFlag = threading.Event()

    def start(self):
        self.dispatcher.add_handler(self.handler)
        self.updater.start_polling()
        print("started")

    def __calback(self, msg):
        '''
        Once the message is received end polling
        and set the event flag.
        This allows us to simply wait for the message.
        '''
        self.msg = msg
        print("trying to end polling")
        print(self.dispatcher.running)
        self.dispatcher.stop()
        print(self.dispatcher.running)
        self.updater.stop()
        print("ended polling")
        self.callbackFlag.set()
        print("set flag")
