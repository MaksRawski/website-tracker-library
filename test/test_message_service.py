import unittest
import yaml
import asyncio
import os

from sas.message_service import MessageService
from .verify import VerifyingBot

TEST_CONFIG = os.environ['TEST_CONFIG']

class integration(unittest.TestCase):
    def setUp(self):
        with open(TEST_CONFIG, "r") as f:
            config = yaml.safe_load(f)

        self.messageService = MessageService(token=config['serviceBotToken'], cid=config['cid'])
        self.verifyingBot = VerifyingBot(token=config['verifyingBotToken'], cid=config['cid'])
        self.loop = asyncio.get_event_loop()


    def test_messageService(self):
        self.verifyingBot.start()

        '''
        this function is normally run asynchronously in a non-blocking fashion
        however we want to test whether after it's done the result is what we want
        therefore we have to wait until it's complete.
        '''
        self.loop.run_until_complete(self.messageService.send("this is a messageService test"))

        '''
        verifyingBot.msg is set to the last message
        the bot has seen on the channel given by cid.
        We wait until the callbackFlag is set,
        in other words until there is a new message.
        '''
        self.verifyingBot.callbackFlag.wait(timeout=10)
        self.assertEqual(self.verifyingBot.msg, "this is a messageService test")


    def tearDown(self):
        self.loop.close()
