from website_tracker import Tracker, WebsiteDefinition, Credentials, findByText
import unittest
from unittest.mock import MagicMock
import yaml
import os

TEST_CONFIG = os.environ['TEST_CONFIG']
SAMPLE_WEBSITE = os.environ['SAMPLE_WEBSITE']

# TODO: move this function to lib
# rename it to initFromYAML or sth
def loadConfig(path: str):
    with open(TEST_CONFIG, 'r') as f:
        cfg = yaml.safe_load(f.read())
        return cfg

class test(unittest.TestCase):
    def setUp(self):

        website = WebsiteDefinition(
            name = 'test',
            loginUrl = 'https://reddit.com',
            loginPrompt = findByText('Log In'),
            loginSelector = '#loginUsername',
        )
        cfg = loadConfig(TEST_CONFIG)
        website = loadConfig(SAMPLE_WEBSITE)

        creds = Credentials(
            username = website['username'],
            password = website['password'],
            telegramToken = cfg['serviceBotToken'],
            cid = cfg['cid']
        )

        self.tracker = Tracker(website, creds, logging=True)
        self.tracker.log = MagicMock()

    def test_data(self):
        self.assertTrue( self.tracker.verifyData() )

    def test_getLoginForm(self):
        loginForm = self.tracker.getLoginForm()
        self.assertEqual(loginForm.get_attribute("id"), 'loginUsername')

    @unittest.skip("not implemented yet")
    def test_login(self):
        # we mock it and manually switch iframes
        self.tracker.getLoginForm = MagicMock(return_value=...)
        self.tracker.getLoginForm.assert_called_with(...)

        self.tracker.login()
        self.tracker.log.assert_called_with(f"{self.tracker.creds['username']} logged in.")

    def tearDown(self):
        self.tracker.driver.close()
        self.tracker.driver.quit()

if __name__ == '__main__':
    unittest.main()
