# SAS - Scrape And Send

![logo](sas.png)

### _Evict boilerplate out of your scrape and send projects with SAS!_
It's like scrapy but with built-in notifying via telegram channel.

# Tests

[![pipeline status](https://gitlab.com/MaksRawski/website-tracker-library/badges/master/pipeline.svg)](https://gitlab.com/MaksRawski/website-tracker-library/-/commits/develop)

## To test scraping:

~~_reddit is used as a sample website to test the library and user karma is used as a tracking object_~~

1. Put `username` and `password` into `sample_website.yml`
2. Run `python -m unittest test.test_scraping`.


## To test MessageService:

0. Create 2 test accounts and a telegram channel.
1. Put `cid`, `serviceBotToken` and `verifyingBotToken` to `test_config.yml`.
	- `cid` can be obtained using [this method](https://gist.github.com/mraaroncruz/e76d19f7d61d59419002db54030ebe35).
2. Run `python -m unittest test.test_message_service`.


# Type stubs

### For selenium:
`pyright --createstub selenium`

### For [python-telegram-bot](https://github.com/python-telegram-bot/python-telegram-bot):
`pyright --createstub telegram`


# TODO:
- [x] try to make a simple release of this
	- [x] remove unused functions
	- [x] clean up
	- [x] merge to master
	- [x] come up with a cool name
	- [x] make a cool logo with August III Sas
- [ ] modify self.start to not break the first time a random exception occurs
- [ ] improve the way self._start looks
- [ ] make CI happy
	- [x] add CI status
- [x] `test_config.yaml`
- [ ] `sample_website.yaml`
- [ ] (test.message_service) fix never ending polling bug
- [x] implement whole pipeline of BaseTracker
	- [ ] test it
- [ ] write a sample integration test for BaseTracker
- [ ] move trackers to seperate files

### Long term goals:
- [ ] add option to use pandas dataframes instead of lists of strings
